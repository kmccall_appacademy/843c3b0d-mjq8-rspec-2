def adder(num= 1, &block)
  block.call + num
end

def reverser(&block)
  result = []
  block.call.split.each do |word|
    result << word.reverse
  end
  result.join(" ")
end

def repeater(repeats= 1, &block)
  repeats.times do
    block.call
  end
end
