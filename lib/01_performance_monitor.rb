def measure(runs=1, &block)
  start_time = Time.now
  runs.times do
  block.call
  end
  finish_time = Time.now
  elapsed_time = (finish_time - start_time)/runs
end
